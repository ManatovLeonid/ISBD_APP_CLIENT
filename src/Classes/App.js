import logo from '../logo.svg';
import './App.css';
import '../config.js';
import React, {Component} from "react";
import Login from "./Login";
import Profile from "./Profile/Profile";
import Search from "./Search/Search";
import Chats from "./Chats/Chats";
import {cardJson, chatJson, profileJson} from "../config";
import NavLinkState from "./NavLinkState";
import {GetCard, GetChats, GetUser} from "./ServerAPI";
import Coupons from "./Coupons/Coupons";

const STATE_LOGIN = 0;
const STATE_SEARCH = 1;
const STATE_CHATS = 2;
const STATE_CHATTING = 3;
const STATE_MYPROFILE = 4;
const STATE_COUPONS = 5;


class App extends Component {

    ChangeState(newAppState) {
        this.setState({appState: newAppState});
    }

    ChangeLoggedIn(isLoggedIn) {


        this.setState({isLogin: localStorage.setItem('isLogin', isLoggedIn)});


        this.setState({isLogin: isLoggedIn});
        if (isLoggedIn === true) {

            this.ChangeState(STATE_MYPROFILE);

        } else {

            this.ChangeState(STATE_LOGIN);
            this.SetUser(null);
        }


    }

    SetUser(user) {
        this.setState({currentUser: user});
        if (user != null) {

            this.setState({currentUserId: user.id});
            localStorage.setItem('currentUserId', user.id)
        } else {


            this.setState({currentUserId: null});
            localStorage.setItem('currentUserId', null)
        }
    }

    constructor(props) {
        super(props);
        console.log(props);

        this.state = {appState: localStorage.getItem('appState')};
        this.state = {isLogin: localStorage.getItem('isLogin')};
        this.state = {currentUserId: localStorage.getItem('currentUserId')};

        if (localStorage.getItem('isLogin') == null) this.state = {isLogin: false};

        if (localStorage.getItem('isLogin') === true) {
            this.SetUser(GetUser(localStorage.getItem('currentUserId')))
        }

        this.OnLogin = this.OnLogin.bind(this);
        this.ChangeState = this.ChangeState.bind(this);
        this.SwitchAppState = this.SwitchAppState.bind(this);
        this.ChangeLoggedIn = this.ChangeLoggedIn.bind(this);
        this.SetUser = this.SetUser.bind(this);
    }


    OnLogin(user) {
        this.SetUser(user);
        this.ChangeLoggedIn(true);
    }


    SwitchAppState(appState) {

        localStorage.setItem('appState', appState);
        switch (appState) {
            case STATE_LOGIN:
                return (<Login login={""} isLogin={this.state.isLogin} logOut={() => this.ChangeLoggedIn(false)}
                               OnLogin={this.OnLogin}/>);
            case STATE_SEARCH:
                return (<Search userCard={GetCard(this.state.currentUser.id)}/>);
            case STATE_CHATS:
                return (<Chats userid={this.state.currentUser.id}/>);
            case STATE_MYPROFILE:
                return (<Profile userid={this.state.currentUser.id}/>);
            case STATE_COUPONS:
                return (<Coupons userid={this.state.currentUser.id}/>);

        }

    }

    render() {

        if (this.state.isLogin == false || this.state.isLogin == undefined) {
            this.state.appState = STATE_LOGIN;
            return (
                <div className="App">

                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <div className="container-fluid">
                            <a className="navbar-brand" href="#">Носов, Манатов P33111</a>
                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                    aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarNav">
                                <ul className="navbar-nav">

                                    <NavLinkState currentActive={this.state.appState} state={STATE_LOGIN} text={"Login"}
                                                  ChangeState={this.ChangeState}/>

                                </ul>
                            </div>
                        </div>
                    </nav>


                    <div className="pt-3">

                        {this.SwitchAppState(this.state.appState)}
                    </div>


                </div>
            )
        }


        return (
            <div className="App ">


                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#">Носов, Манатов P33111</a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav">

                                <NavLinkState currentActive={this.state.appState} state={STATE_LOGIN} text={"Login"}
                                              ChangeState={this.ChangeState}/>
                                <NavLinkState currentActive={this.state.appState} state={STATE_SEARCH} text={"Search"}
                                              ChangeState={this.ChangeState}/>
                                <NavLinkState currentActive={this.state.appState} state={STATE_CHATS} text={"Chats"}
                                              ChangeState={this.ChangeState}/>
                                <NavLinkState currentActive={this.state.appState} state={STATE_COUPONS} text={"Coupons"}
                                              ChangeState={this.ChangeState}/>
                                <NavLinkState currentActive={this.state.appState} state={STATE_MYPROFILE}
                                              text={"Profile"}
                                              ChangeState={this.ChangeState}/>


                            </ul>
                        </div>
                    </div>
                </nav>


                {/*<ul className="nav justify-content-center nav-pills">*/}

                {/*    <NavLinkState currentActive={this.state.appState} state={STATE_LOGIN} text={"Login"}*/}
                {/*                  ChangeState={this.ChangeState}/>*/}
                {/*    <NavLinkState currentActive={this.state.appState} state={STATE_SEARCH} text={"Search"}*/}
                {/*                  ChangeState={this.ChangeState}/>*/}
                {/*    <NavLinkState currentActive={this.state.appState} state={STATE_CHATS} text={"Chats"}*/}
                {/*                  ChangeState={this.ChangeState}/>*/}
                {/*    <NavLinkState currentActive={this.state.appState} state={STATE_COUPONS} text={"Coupons"}*/}
                {/*                  ChangeState={this.ChangeState}/>*/}
                {/*    <NavLinkState currentActive={this.state.appState} state={STATE_MYPROFILE} text={"Profile"}*/}
                {/*                  ChangeState={this.ChangeState}/>*/}

                {/*</ul>*/}

                {/*<button onClick={()=>{this.ChangeState(STATE_LOGIN)}}>STATE_LOGIN</button>*/}
                {/*<button onClick={()=>{this.ChangeState(STATE_SEARCH)}}>STATE_SEARCH</button>*/}
                {/*<button onClick={()=>{this.ChangeState(STATE_CHATS)}}>STATE_CHATS</button>*/}
                {/*<button onClick={()=>{this.ChangeState(STATE_MYPROFILE)}}>STATE_MYPROFILE</button>*/}


                <div className="pt-3">

                    {this.SwitchAppState(this.state.appState)}
                </div>

                {/*{this.state.appState}*/}
                {/*{localStorage.setItem('myCat', 'Tom')}*/}


            </div>
        );
    }

}


export default App;
