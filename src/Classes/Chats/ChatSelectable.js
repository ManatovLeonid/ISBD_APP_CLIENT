import React, {Component} from "react";
import Message from "./Message";
import {chatJson, femaleImgSrc, male, maleImgSrc, userJson} from "../../config";
import {GetCard} from "../ServerAPI";

class ChatSelectable extends Component {

    constructor(props) {
        super(props);
        this.DrawIcon = this.DrawIcon.bind(this);
        this.state = {loaded: false, chatCard: chatJson};
    }


    componentDidMount() {

        let chatCardId;

        if (this.props.chat.card_b === this.props.userCard.user.id) {
            chatCardId = this.props.chat.card_a;
        } else {
            chatCardId = this.props.chat.card_b;
        }


        GetCard(chatCardId, (chatCard) => {

            this.setState({"chatCard": chatCard, "loaded": true});

        });
    }

    DrawIcon(sex) {

        let imgStyle = {"height": "200px", margin: "0px 0px -80px 0px ", "clip-path": "circle(60px at 50% 60px)"};
        let divStyle = {overflow: "hidden"};
        if ((sex === male)) return <div style={divStyle}><img alt="male" style={imgStyle}
                                                              src={maleImgSrc}/></div>;
        else return <div style={divStyle}><img alt="female" style={imgStyle} src={femaleImgSrc}/></div>;
    }


    render() {

        if (this.state.loaded === false) {

            return (<div className="ChatSelectable m-2 "><div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div></div> );
        }



        console.log(this.props.chat);
        return (
            <div className="ChatSelectable m-2 ">

                <button type="button" className="btn  btn-outline-dark " onClick={() => this.props.OnOpenClick()}>
                    {this.DrawIcon(this.state.chatCard.sex)}
                    <p>{this.state.chatCard.name}</p>
                    {/*<p>{this.props.chat.card_a} {this.props.chat.state} </p>*/}
                </button>

            </div>
        );
    }
}


export default ChatSelectable;