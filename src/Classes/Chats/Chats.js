import React, {Component} from "react";
import ChatSelectable from "./ChatSelectable";
import Chatting from "./Chatting";
import {GetCard, GetChats} from "../ServerAPI";
import {userJson} from "../../config";

class Chats extends Component {

    constructor(props) {
        super(props);


        this.state = {loaded: 0, selectedChatNum: -1, chatsObject: {count: 0, chats: []}, userCard: userJson};
        // this.state = {chatsCount: this.props.chatsObject.count};

        this.OnChatSelect = this.OnChatSelect.bind(this);
        this.CheckChatDraw = this.CheckChatDraw.bind(this);
        this.DrawSelectableChats = this.DrawSelectableChats.bind(this);
        this.DrawConcreteChat = this.DrawConcreteChat.bind(this);


    }


    componentDidMount() {
        this.setState({"loaded": this.state.loaded - 2});

        GetChats(this.props.userid, (chatsObj) => {

            this.setState({"chatsObject": chatsObj, "loaded": this.state.loaded + 1});

        });

        GetCard(this.props.userid, (userCard) => {

            this.setState({"userCard": userCard, "loaded": this.state.loaded + 1});

        });


    }

    OnChatSelect(chatNum) {
        this.setState({selectedChatNum: chatNum})
    }

    CheckChatDraw() {
        if (this.state.selectedChatNum == -1 || this.state.selectedChatNum == undefined) return this.DrawSelectableChats();
        else return this.DrawConcreteChat(this.state.selectedChatNum);
    }

    DrawSelectableChats() {

        console.log(this.state.loaded);
        if (this.state.loaded === 0) {
            let returns = [];


            for (let i = 0; i < this.state.chatsObject.count; i++) {

                returns.push(<ChatSelectable  userCard={this.state.userCard}
                                             chat={this.state.chatsObject.chats[i]}
                                             OnOpenClick={() => this.OnChatSelect(i)}/>);
            }

            return (<div className="container d-flex justify-content-center align-items-start flex-wrap flex-row"
                         style={{"max-width": "500px"}}> {returns}</div>);
        }


        return (<div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
        </div>);


        //
        // return <div>
        //     <ChatSelectable OnOpenClick={()=>this.OnChatSelect(0)}/>
        //     <ChatSelectable OnOpenClick={()=>this.OnChatSelect(1)}/>
        //     <ChatSelectable OnOpenClick={()=>this.OnChatSelect(2)}/>
        //     <ChatSelectable OnOpenClick={()=>this.OnChatSelect(3)}/>
        //     <ChatSelectable OnOpenClick={()=>this.OnChatSelect(4)}/>
        // </div>;
    }

    DrawConcreteChat(chatNum) {
        return <div><Chatting userCard={this.state.userCard} chat={this.state.chatsObject.chats[chatNum]}
                              chatNum={chatNum}/>
            <button className="btn btn-outline-danger" onClick={() => {
                this.setState({selectedChatNum: -1})
            }}>CloseChat
            </button>
        </div>;
    }


    render() {
        return (
            <div className="Chats">
                <h2>Chats</h2>

                {this.CheckChatDraw()}


            </div>
        );
    }
}


export default Chats;