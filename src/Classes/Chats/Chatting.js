import React, {Component} from "react";
import Message from "./Message";
import {chatJson, messagesJson} from "../../config";
import ChatSelectable from "./ChatSelectable";
import {GetCard, GetMessages, SendMessage} from "../ServerAPI";
import {uuid4} from "uuid4/browser";


class Chatting extends Component {

    constructor(props) {
        super(props);
        this.DrawMessages = this.DrawMessages.bind(this);
        this.SendMessage = this.SendMessage.bind(this);
        this.state = {loaded: 0, chatCard: chatJson, messages: null, idemp_key: uuid4()};
    }


    componentDidMount() {

        if (this.state.loaded === 2) this.setState({"loaded": 0});

        let chatCardId;

        if (this.props.chat.card_b === this.props.userCard.user.id) {
            chatCardId = this.props.chat.card_a;
        } else {
            chatCardId = this.props.chat.card_b;
        }


        GetCard(chatCardId, (chatCard) => {

            this.setState({"chatCard": chatCard, "loaded": this.state.loaded + 1});
            GetMessages(this.props.chat.id, (messages) => this.setState({
                "messages": messages,
                "loaded": this.state.loaded + 1
            }));
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        let inp = document.getElementById("message");
        if (inp !== null) inp.value = "";
    }

    DrawMessages(messages) {
        let returns = [];


        for (let i = 0; i < messages.length; i++) {

            returns.push(<Message my={this.props.userCard.user.id === messages[i].user} type={messages[i].message_type}
                                  authorCard={this.props.userCard.user.id === messages[i].user ? this.props.userCard : this.state.chatCard}
                                  chat={this.props.chat} text={messages[i].text}/>);
        }

        return (<div> {returns}</div>);
    }

    SendMessage() {
        let text = document.getElementById("message").value;

        SendMessage({text: text, idemp_key: this.state.idemp_key}, this.props.chat.id, (res) => {


            this.state.messages.push(res);
            this.setState({idemp_key: uuid4()});


        });
    }

    render() {


        if (this.state.loaded < 2) {

            return (<div className="ChatSelectable m-2 ">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>);
        }

        return (
            <div className="Chatting container" style={{"max-width": "700px"}}>
                <p>Chatting with {this.state.chatCard.name}</p>


                <div style={{height: "500px","overflow-y":"auto","background":"url(img/message_bg.jpg) no-repeat center","background-size":"cover"}} className="p-3">
                    {this.DrawMessages(this.state.messages)}
                </div>

                <div className="input-group mb-3">
                    <input type="text" className="form-control" placeholder="message"
                           aria-label="Recipient's username" aria-describedby="button-addon2" id="message"/>
                    <div className="input-group-append">
                        <button className="btn btn-success" type="button" id="button-addon2"
                                onClick={this.SendMessage}>Send
                        </button>
                    </div>
                </div>


            </div>
        );
    }
}


export default Chatting;