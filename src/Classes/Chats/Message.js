import React, {Component} from "react";

class Message extends Component {

    constructor(props) {
        super(props);
    }


    render() {
        let text = this.props.text;
        let author = "";
        let classNames = ["Message", "d-flex", "align-items-center", "py-2"];


        if (this.props.my === true) {
            classNames.push("justify-content-start flex-row-reverse");
            author = "you";

        } else if (this.props.type === 1) {
            classNames.push("justify-content-center flex-row");
            author = "server";

        } else {
            classNames.push("justify-content-start flex-row");
            author = this.props.authorCard.name;

        }
        return (

            <div className={classNames.join(" ")}>
                <div className="border d-inline-flex bg-light px-2" style={{"flex-direction":"inherit","border-radius":"20px"}}>
                    <div className="d-inline-block p-2 font-weight-bold"> {author}</div>
                    <div className="d-inline-block  p-2"> {text}</div>
                </div>
            </div>
        );
    }
}


export default Message;