import React, {Component} from "react";
import {GetCard, GetChats, GetCoupons} from "../ServerAPI";

class Coupons extends Component {

    constructor(props) {
        super(props);

        this.state = {loaded: 0, coupons: []};
        this.DrawCoupons = this.DrawCoupons.bind(this);

    }


    componentDidMount() {
        this.setState({"loaded": this.state.loaded - 1});

        GetCoupons(this.props.userid, (coupons) => {

            this.setState({"coupons": coupons, "loaded": this.state.loaded + 1});

        });


    }


    DrawCoupons() {

        let returns = [];
        let coupons = this.state.coupons;
        for (let i = 0; i < coupons.length; i++) {

            returns.push(<div className=" btn btn-success m-2 "
                              style={{"border-radius": "20px"}}>{coupons[i]}</div>);
        }

        return (<div className="d-inline-flex flex-column  justify-content-center"> {returns}</div>);

    }

    render() {


        if (this.state.loaded !== 0) {

            return (<div className="ChatSelectable m-2 ">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>);
        }


        return (
            <div className="Coupons">
                <h2>Coupons</h2>

                {this.DrawCoupons()}

            </div>
        );
    }
}


export default Coupons;