import React, {Component} from "react";
import {GetUser} from "./ServerAPI";

class Login extends Component {

    constructor(props) {
        super(props);
        this.TryLogin = this.TryLogin.bind(this);
    }

    async TryLogin() {
        let user = await GetUser(document.getElementById("loginMail").value);
        if (user != null) {
            this.props.OnLogin(user);
        }
    }

    render() {


        if (this.props.isLogin) {
            return (<div className="Login">
                <h2>Login</h2>
                <p>KyNLzWPytKwfei@yay.website</p>
                <p>smGLRiBLVjbRTN@yay.website</p>
                <p>UbTFFIWzbttXje@yay.website</p>

                <input type="text" className="form-control" id="loginMail"
                       value={this.props.login}/>
                <button className="btn btn-danger" onClick={() => this.props.logOut()}>LogOut</button>
            </div>)
        }

        return (
            <div className="Login">

                <h2>Login</h2>
                <p>KyNLzWPytKwfei@yay.website</p>
                <p>smGLRiBLVjbRTN@yay.website</p>
                <p>UbTFFIWzbttXje@yay.website</p>
                <input type="text" className="form-control" id="loginMail"
                />
                <button className="btn btn-success" onClick={this.TryLogin}>Login</button>


            </div>
        );
    }
}


export default Login;