import React, {Component} from "react";
import Message from "./Chats/Message";

class NavLinkState extends Component {

    constructor(props) {
        super(props);
    }


    render() {

        let classNames = "nav-link";
        if (this.props.currentActive === this.props.state)
            classNames += " active border";


        return (
            <li className="nav-item ">
                <a href="#" className={classNames} style={{"border-radius":"20px"}} onClick={() => {
                    this.props.ChangeState(this.props.state);
                }}>
                    {this.props.text}

                </a>
            </li>
        );
    }
}


export default NavLinkState;