import React, {Component} from "react";

class NameInput extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="NameInput container col-6" >
                <label htmlFor="exampleInputEmail1">Name</label>
                <input type="text" className="form-control" id="exampleInputEmail1" />
            </div>
        );
    }
}


export default NameInput;