import React, {Component} from "react";
import NameInput from "./NameInput";
import {female, male, userJson} from "../../config";
import {GetAllRests, GetCard, GetChats, GetRes, SendNewProfile} from "../ServerAPI";
import ChatSelectable from "../Chats/ChatSelectable";
import Message from "../Chats/Message";

class Profile extends Component {

    constructor(props) {
        super(props);
        this.ProfileUpdate = this.ProfileUpdate.bind(this);
        this.DrawRestsSelect = this.DrawRestsSelect.bind(this);
        this.state = {userCard: userJson, loaded: 0, res1: null, res2: null, res3: null, "allRests": []};
    }

    componentDidMount() {

        if (this.state.loaded === 5) this.setState({"loaded": 0});


        GetCard(this.props.userid, (userCard) => {

            this.setState({"userCard": userCard, "loaded": this.state.loaded + 1});
            console.log(userCard);
            GetRes(userCard.res1_id, (res) => this.setState({"res1": res, "loaded": this.state.loaded + 1}));
            GetRes(userCard.res2_id, (res) => this.setState({"res2": res, "loaded": this.state.loaded + 1}));
            GetRes(userCard.res3_id, (res) => this.setState({"res3": res, "loaded": this.state.loaded + 1}));
            GetAllRests((res) => this.setState({"allRests": res, "loaded": this.state.loaded + 1}));

        });
    }


    DrawRestsSelect(id, value) {
        let returns = [];
        let rests = this.state.allRests;

        for (let i = 0; i < rests.length; i++) {
            returns.push(<option value={rests[i].id}> {rests[i].title} </option>);
        }

        return (<select className="form-control" id={id} defaultValue={value}> {returns}</select>);

    }

    ProfileUpdate() {

        let name = document.getElementById("profileName").value;
        let about = document.getElementById("about").value;
        let age = document.getElementById("profileAge").value;
        let sex = document.querySelector('input[name="sexRadio"]:checked').value;
        let res1 = document.getElementById("res1").value;
        let res2 = document.getElementById("res2").value;
        let res3 = document.getElementById("res3").value;

        let search_age_from = document.getElementById("searchAgeFrom").value;
        let search_age_to = document.getElementById("searchAgeTo").value;
        let search_sex_arr = document.querySelectorAll('input[name="searchSexRadio"]:checked');

        let search_sex = search_sex_arr.length === 1 ? search_sex_arr[0].value : "";

        let obj = {
            about: about,
            name: name,
            birthday: age,
            sex: sex,
            res1_id: res1,
            res2_id: res2,
            res3_id: res3,
            search_age_from: search_age_from,
            search_age_to: search_age_to,
            search_sex: search_sex
        };

        SendNewProfile(obj);

    }

    render() {


        if (this.state.loaded < 5) {
            return (<div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>);
        }


        let imgSrc = "./../img/";
        if (this.state.userCard.sex === male) imgSrc += "male.png";
        else imgSrc += "female.png";

        return (
            <div className="Profile container d-flex flex-column justify-content-center align-items-center">

                <div className="">
                    <h2>Profile</h2>

                    <img src={imgSrc} alt="profile pic"/>

                    <div>


                        <label htmlFor="profileName">Name</label>
                        <input type="text" className="form-control" id="profileName"
                               defaultValue={this.state.userCard.name}/>

                        <label htmlFor="profileName">About</label>
                        <textarea className="form-control" id="about"
                                  defaultValue={this.state.userCard.about}/>

                        <label htmlFor="profileAge">Birthday</label>
                        <input type="text" className="form-control" id="profileAge"
                               defaultValue={this.state.userCard.birthday}/>

                        <label htmlFor="sexRadio">Sex</label>

                        <div className="form-check">
                            <input className="form-check-input" type="radio" name="sexRadio" id="sexRadio1"
                                   value={male} defaultChecked={this.state.userCard.sex === male}
                            />
                            <label className="form-check-label" htmlFor="sexRadio1">
                                male
                            </label>
                        </div>


                        <div className="form-check">
                            <input className="form-check-input" type="radio" name="sexRadio" id="sexRadio2"
                                   value={female} defaultChecked={this.state.userCard.sex === female}/>
                            <label className="form-check-label" htmlFor="sexRadio2">
                                female
                            </label>
                        </div>


                        <label htmlFor="res1">restaurant 1</label>
                        {this.DrawRestsSelect("res1", this.state.res1.id)}
                        <label htmlFor="res2">restaurant 2</label>
                        {this.DrawRestsSelect("res2", this.state.res2.id)}
                        <label htmlFor="res3">restaurant 3</label>
                        {this.DrawRestsSelect("res3", this.state.res3.id)}

                        {/*<label htmlFor="res1">restaurant 1</label>*/}
                        {/*<input type="text" className="form-control" id="res1"*/}
                        {/*       defaultValue={this.state.res1.title}/>*/}

                        {/*<label htmlFor="res2">restaurant 2</label>*/}
                        {/*<input type="text" className="form-control" id="res2"*/}
                        {/*       defaultValue={this.state.res2.title}/>*/}

                        {/*<label htmlFor="res3">restaurant 3</label>*/}
                        {/*<input type="text" className="form-control" id="res3"*/}
                        {/*       defaultValue={this.state.res3.title}/>*/}


                        <button className="btn btn-dark" onClick={this.ProfileUpdate}>Submit</button>
                    </div>


                </div>

                <div className="m-3 p-3">
                    <h2>Search Options</h2>
                    <div>
                        <p>Sex</p>
                        <div className="form-check ">
                            <input className="form-check-input" type="checkbox" name="searchSexRadio"
                                   id="searchSexRadio1"
                                   value={male} defaultChecked={this.state.userCard.search_sex === male || this.state.userCard.search_sex === null}
                            />
                            <label className="form-check-label" htmlFor="exampleRadios1">
                                male
                            </label>
                        </div>


                        <div className="form-check">
                            <input className="form-check-input" type="checkbox" name="searchSexRadio"
                                   id="searchSexRadio2"
                                   value={female} defaultChecked={this.state.userCard.search_sex === female|| this.state.userCard.search_sex === null}/>
                            <label className="form-check-label" htmlFor="exampleRadios2">
                                female
                            </label>


                            <p>Age</p>
                            <label htmlFor="searchAgeFrom">From</label>
                            <input type="text" className="form-control m-auto w-auto" id="searchAgeFrom"
                                   defaultValue={this.state.userCard.search_age_from}/>

                            <label htmlFor="searchAgeTo">To</label>
                            <input type="text" className="form-control m-auto w-auto " id="searchAgeTo"
                                   defaultValue={this.state.userCard.search_age_to}/>
                        </div>
                        <button onClick={this.ProfileUpdate} className=" align-self-center btn btn-dark">Submit
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}


export default Profile;