import React, {Component} from "react";
import {femaleImgSrc, male, maleImgSrc} from "../../config";
import {GetRes} from "../ServerAPI";

class OfferCard extends Component {

    constructor(props) {
        super(props);
    }


    render() {
        let imgSrc = "";
        if (this.props.card.sex === male) imgSrc = maleImgSrc;
        else imgSrc = femaleImgSrc;


        return (
            <div className="d-flex flex-row align-content-center justify-content-center">

                <div className="card" style={{width: "370px"}}>
                    <img src={imgSrc} className="card-img-top m-auto w-auto" alt="card pic"/>
                    <div className="card-body">
                        <h5 className="card-title">{this.props.card.name}</h5>
                        <p className="card-text">{this.props.card.about}</p>
                        <p className="card-text">{this.props.card.user.mail}</p>
                    </div>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">{this.props.res1.title}</li>
                        <li className="list-group-item">{this.props.res2.title}</li>
                        <li className="list-group-item">{this.props.res3.title}</li>
                    </ul>
                    <div className="card-body">

                        <button className="btn btn-danger" onClick={this.props.deny}>Deny</button>
                        <button className="btn btn-success" onClick={this.props.offer}>Offer</button>
                        {/*<a href="#" className="card-link">Offer</a>*/}
                        {/*<a href="#" className="card-link">Deny</a>*/}
                    </div>
                </div>

            </div>
        );
    }
}


export default OfferCard;