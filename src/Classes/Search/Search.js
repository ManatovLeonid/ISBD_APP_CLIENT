import React, {Component} from "react";
import OfferCard from "./OfferCard";
import {cardJson} from "../../config";
import {DenyPair, GetCard, GetMessages, GetNextCard, GetRes, OfferPair} from "../ServerAPI";

class Search extends Component {

    constructor(props) {
        super(props);
        this.OnOfferClick = this.OnOfferClick.bind(this);
        this.OnDenyClick = this.OnDenyClick.bind(this);
        this.state = {currentCard: null, loaded: false, emptySearch: false, res1: null, res2: null, res3: null};
    }


    componentDidMount() {

        if (this.state.loaded === false) this.state.loaded = 0;
        this.setState({"loaded": this.state.loaded - 4});


        GetNextCard((userCard) => {


            if (userCard.length === 0) {
                this.setState({"emptySearch": true});
                return;
            }

            this.setState({"currentCard": userCard[0], "loaded": this.state.loaded + 1});
            console.log(userCard);
            GetRes(userCard[0].res1_id, (res) => this.setState({"res1": res, "loaded": this.state.loaded + 1}));
            GetRes(userCard[0].res2_id, (res) => this.setState({"res2": res, "loaded": this.state.loaded + 1}));
            GetRes(userCard[0].res3_id, (res) => this.setState({"res3": res, "loaded": this.state.loaded + 1}));

        });

    }

    OnDenyClick() {
        DenyPair(this.state.currentCard.user.id, () => {


            setTimeout(() => {

                this.forceUpdate();
                this.setState({state: this.state});
                this.componentDidMount();
            }, 500);
        });

    }

    OnOfferClick() {
        OfferPair(this.state.currentCard.user.id, () => {


            setTimeout(() => {

                this.forceUpdate();
                this.setState({state: this.state});
                this.componentDidMount();
            }, 500);
        });
    }


    render() {


        if (this.state.emptySearch === true) {
            return (<div className="Search">
                <h2>Search</h2>
               <p className="text-info">Not Found</p>
            </div>);
        }
        if (this.state.loaded !== 0) {
            return (<div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>);
        }


        // this.state = {currentCard: this.state.currentCard};

        return (
            <div className="Search">
                <h2>Search</h2>
                <OfferCard deny={this.OnDenyClick} offer={this.OnOfferClick} card={this.state.currentCard}
                           res1={this.state.res1} res2={this.state.res2} res3={this.state.res3}/>
            </div>
        );
    }
}


export default Search;