import {card1Json, cardJson, chatJson, messagesJson, resJson, serverIP, userJson} from "../config";
import axios from 'axios';

async function Get(url) {

    let response = await axios({
        method: 'get',
        url: serverIP + "/" + url,
        withCredentials: true
    });
    console.log(url);
    console.log(response.data.data);
    return response.data.data;

}

async function Delete(url) {

    let response = await axios({
        method: 'Delete',
        url: serverIP + "/" + url,
        withCredentials: true
    });
    console.log(url);
    console.log(response.data.data);
    return response.data.data;

}

async function Put(url, data) {

    let response = await axios({
        method: 'put',
        url: serverIP + "/" + url,
        withCredentials: true,
        data: data
    });
    console.log(url);
    console.log(response.data.data);
    return response.data.data;

}

async function Post(url, data) {

    let response = await axios({
        method: 'post',
        url: serverIP + "/" + url,
        withCredentials: true,
        data: data
    });
    console.log(url);
    console.log(response.data.data);
    return response.data.data;

}


export async function GetRes(res_id, callback = () => {
}) {


    let res = await Get("restaurant/" + res_id).then(res => callback(res));
    return res;

}

export function GetChats(user_id, callback) {

    Get("pairs").then(res => callback({count: res.length, chats: res}));

}

export function GetCoupons(user_id, callback) {

    Get("coupons/my").then(res => callback(res));

}

export function SendMessage(data,pair_id, callback) {

    Post("pair/" + pair_id + "/messages",data).then(res => callback(res));

}

export function GetAllRests(callback) {

    Get("restaurants").then(res => callback(res));

}

export function GetNextCard(callback) {
    Get("search").then(res => callback(res));
}

export async function GetCard(user_id, callback = () => {
}) {

    console.log("GetCard");

    if (user_id === 13) {
        return card1Json;
    }
    let res = await Get("card/" + user_id);
    callback(res);
    return res;
}


export function OfferPair(card_id, callback) {
    Put("search/" + card_id, null).then(callback());
}

export function DenyPair(card_id, callback) {
    Delete("search/" + card_id).then(callback());
}

export async function GetUser(login) {

    console.log("GetUser");
    return {id: await Get("user/" + login), mail: login};
}

export async function GetMessages(pair_id, callback) {

    let res = await Get("pair/" + pair_id + "/messages");
    callback(res);
    return res;

    return messagesJson;
}


export async function SendNewProfile(newProfile) {

    // newProfile.res1_id = 1;
    // newProfile.res2_id = 1;
    // newProfile.res3_id = 1;
    Put("card/my", newProfile);


}
