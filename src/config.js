export const serverIP = "https://25.45.63.230:8080";

export const male = true;
export const female = false;

export const maleImgSrc = "/img/male.png";
export const femaleImgSrc = "/img/female.png";


export const chatJson = {id: 333, status: 2, card_a:13, card_b:123};

export const resJson = {title: "rest name"};


export const cardJson = {
    id: 123,
    name: "test name",
    sex: true,
    birthday: "birthday Date",
    about: "lorem ipsum",
    res1: 1,
    res2: 2,
    res3: 3,
    search_age_from: 20,
    search_age_to: 25,
    search_sex: false

};


export const userJson = {
    id: 13,
    mail: "123@123.123"
};
export const messagesJson = [{id:0,user_id:13,text:"Hello!"},{id:0,user_id:123,text:"Hello!!!"}];


export const card1Json = {
    id: 13,
    name: "test name2",
    sex: true,
    birthday: "birthday Date",
    about: "lorem ipsum",
    res1: 1,
    res2: 2,
    res3: 3,
    search_age_from: 20,
    search_age_to: 25,
    search_sex: false

};